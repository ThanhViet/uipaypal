package com.example.paypal

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.paypal.CardsActivity.config.isLandscap
import kotlinx.android.synthetic.main.cards.*
import kotlinx.android.synthetic.main.cards.group
import kotlinx.android.synthetic.main.cards.rcListCards
import kotlinx.android.synthetic.main.cards_landscap.*


class CardsActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (isLandscap) {

            setContentView(R.layout.cards_landscap)
            btnBack3.setOnClickListener(View.OnClickListener { onBackPressed() })
            rcListCards.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rcListCards.adapter = (CardsAdapter(this))
        }
        else {
            setContentView(R.layout.cards)

            btnBack2.setOnClickListener(View.OnClickListener { onBackPressed() })

            rcListCards.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rcListCards.adapter = (CardsAdapter(this))
        }


    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)


        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isLandscap = true
            setContentView(R.layout.cards_landscap)
            btnBack3.setOnClickListener(View.OnClickListener { onBackPressed() })
            rcListCards.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rcListCards.adapter = (CardsAdapter(this))
        }
        else {
            isLandscap = false
            setContentView(R.layout.cards)

            btnBack2.setOnClickListener(View.OnClickListener { onBackPressed() })

            rcListCards.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rcListCards.adapter = (CardsAdapter(this))
        }
    }


    object config {
        var isLandscap = false
    }
    var y1 : Float = 0.0f
    var y2 : Float = 0.0f
    private val minDis = 200
    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (!isLandscap)
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                y1 = event.y
            }
            MotionEvent.ACTION_UP -> {

                y2 = event.y

                if ((y2-y1) > minDis) {
                    group.visibility = View.VISIBLE
                }

                if ((y1-y2) > minDis) {
                    group.visibility = View.GONE
                }


            }
        }
        return super.onTouchEvent(event)
    }
}