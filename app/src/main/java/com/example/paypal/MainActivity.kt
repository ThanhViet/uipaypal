package com.example.paypal

import android.content.Intent
import android.content.res.Configuration
import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cards.*
import kotlinx.android.synthetic.main.dashboard2.*
import kotlinx.android.synthetic.main.main_activity.*
import java.security.AccessController.getContext
import kotlin.math.abs


class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        btn_dashboard.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        })

        btn_pay.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            startActivity(intent)
        })

        btn_cards.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, CardsActivity::class.java)
            startActivity(intent)
        })

    }
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        CardsActivity.config.isLandscap = newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
    }
}