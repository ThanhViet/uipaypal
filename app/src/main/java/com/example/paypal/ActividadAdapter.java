package com.example.paypal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ActividadAdapter extends RecyclerView.Adapter<ActividadAdapter.AccountViewHolder>{


    private Context mContext;


    public ActividadAdapter(Context context)  {

        this.mContext = context;}


    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.actividad_item, parent, false);

        return new AccountViewHolder(view);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull final AccountViewHolder holder,
                                 final int position) {

        if (position == 0) {
            holder.tvName.setText("El corte inglés");
            holder.tvAmount.setText("-50€");
            holder.tvAmount.setTextColor(Color.parseColor("#1b1b1b"));
        }

        if (position == 3) {
            holder.tvName.setText("El corte inglés");
            holder.tvAmount.setText("-80€");
            holder.tvAmount.setTextColor(Color.parseColor("#1b1b1b"));
        }

        if (position == 6) {
            holder.decorLine.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 7;
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder{


        TextView tvName, tvAmount;
        View decorLine;

        public AccountViewHolder(@NonNull final View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_Mar);
            tvAmount = itemView.findViewById(R.id.actividadAmount);
            decorLine = itemView.findViewById(R.id.decorLine);


        }
    }


}





