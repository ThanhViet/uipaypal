package com.example.paypal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.AccountViewHolder>{


    private Context mContext;


    public CardsAdapter(Context context)  {

        this.mContext = context;}


    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.card_item, parent, false);

        return new AccountViewHolder(view);
    }

    int lastPos = -1;

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull final AccountViewHolder holder,
                                 final int position) {
        if (position == 0) {
            holder.logoMasterCard.setVisibility(View.VISIBLE);
            holder.logoVisa.setVisibility(View.GONE);
            holder.cardName.setText("Mastercard");
            holder.cardNumber.setText("****9889");

        } else {
            holder.logoMasterCard.setVisibility(View.GONE);
            holder.logoVisa.setVisibility(View.VISIBLE);
            holder.cardName.setText("Visa black");
            holder.cardNumber.setText("****8764");
        }

        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.showMore.setVisibility(View.VISIBLE);
                holder.showMore.setFocusable(true);
                lastPos = position;
                notifyDataSetChanged();
            }
        });

        if (position != lastPos)  holder.showMore.setVisibility(View.GONE);

    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder{


        ImageView logoVisa, logoMasterCard;
        ImageView btnMore;
        LinearLayout showMore;
        TextView cardName, cardNumber;

        public AccountViewHolder(@NonNull final View itemView) {
            super(itemView);

            logoVisa = itemView.findViewById(R.id.logoVisa);
            logoMasterCard = itemView.findViewById(R.id.logoMasterCard);
            btnMore = itemView.findViewById(R.id.btnMore);
            showMore = itemView.findViewById(R.id.showMore);
            cardName = itemView.findViewById(R.id.cardName);
            cardNumber = itemView.findViewById(R.id.cardNumber);




        }
    }


}





