package com.example.paypal

import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cards.*
import kotlinx.android.synthetic.main.dashboard2.*
import java.security.AccessController.getContext
import kotlin.math.abs


class DashboardActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard2)

        rcEnvior.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        rcEnvior.adapter = (EnviorAdapter(this))

        rcActividad.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rcActividad.adapter = (ActividadAdapter(this))
    }

}